# -*- encoding: utf-8 -*-
import pytest

from contact.tests.factories import ContactFactory
from example_monitor.tests.factories import DomainNameFactory
from monitor.models import DomainName


@pytest.mark.django_db
def test_check_domain_names_in_database():
    contact = ContactFactory(company_name="KB")
    DomainNameFactory(url="https://www.kbsoftware.co.uk", contact=contact)
    assert ["flowable"] == DomainName.objects.check_domain_names_in_database(
        ["flowable", "www.kbsoftware.co.uk"]
    )


@pytest.mark.django_db
def test_check_domain_names_in_database_ignore_invalid_domain_name():
    contact = ContactFactory(company_name="KB")
    DomainNameFactory(url="https://www.kbsoftware.co.uk", contact=contact)
    assert [] == DomainName.objects.check_domain_names_in_database(
        ["flowable", "www.kbsoftware.co.uk"], ignore_invalid_domain_name=True
    )


@pytest.mark.django_db
def test_check_domain_names_in_database_sub_domain():
    contact = ContactFactory(company_name="KB")
    DomainNameFactory(url="https://www.kbsoftware.co.uk", contact=contact)
    assert [
        "kbsoftware.co.uk"
    ] == DomainName.objects.check_domain_names_in_database(["kbsoftware.co.uk"])


@pytest.mark.django_db
def test_is_match():
    domain_name = DomainNameFactory(
        url="https://www.kbsoftware.co.uk",
        contact=ContactFactory(company_name="KB"),
    )
    assert domain_name.is_match("www.kbsoftware.co.uk") is True


@pytest.mark.django_db
def test_is_match_not():
    domain_name = DomainNameFactory(
        url="https://www.kbsoftware.co.uk",
        contact=ContactFactory(company_name="KB"),
    )
    assert domain_name.is_match("kbsoftware.co.uk") is False
