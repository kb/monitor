# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.core.management import call_command
from django.core.management.base import CommandError
from httmock import all_requests, HTTMock
from unittest import mock

from example_monitor.tests.factories import DomainNameFactory, MonitorFactory
from login.models import SYSTEM_GENERATED
from login.tests.factories import UserFactory
from monitor.models import Monitor


@all_requests
def sparkpost_response(url, request):
    return {
        "status_code": 200,
        "content": {
            "results": [
                {
                    "sending_domain": "kbsoftware.co.uk",
                    "count_sent": 123,
                    "count_bounce": 22,
                },
                {
                    "sending_domain": "pkimber.net",
                    "count_sent": 88,
                    "count_bounce": 23,
                },
            ]
        },
    }


class MockWhoIs:
    expiration_date = date.today()


@pytest.mark.django_db
def test_check_domain_name():
    MonitorFactory(
        slug=Monitor.DOMAIN_NAME,
        app="monitor",
        module="models",
        report_class="DomainName",
    )
    UserFactory(username=SYSTEM_GENERATED)
    with mock.patch("whois.whois") as m:
        m.return_value = MockWhoIs()
        call_command("check-domain-name")


@pytest.mark.django_db
def test_check_sparkpost():
    MonitorFactory(
        slug=Monitor.SPARKPOST,
        app="monitor",
        module="models",
        report_class="SparkPost",
    )
    UserFactory(username=SYSTEM_GENERATED)
    with HTTMock(sparkpost_response):
        call_command("check-sparkpost")


@pytest.mark.django_db
def test_init_app_monitor():
    UserFactory(username=SYSTEM_GENERATED)
    call_command("init_app_monitor")


@pytest.mark.django_db
def test_init_app_monitor_missing_system_generated_user():
    with pytest.raises(CommandError):
        call_command("init_app_monitor")
