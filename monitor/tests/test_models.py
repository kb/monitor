# -*- encoding: utf-8 -*-
import pytest

from monitor.models import hostname_for_url, prepend_url_with_http


@pytest.mark.django_db
def test_host_name_for_url():
    assert "www.kbsoftware.co.uk" == hostname_for_url(
        "https://www.kbsoftware.co.uk"
    )


@pytest.mark.django_db
def test_host_name_for_url_may_be_invalid():
    assert "kbsoftware" == hostname_for_url("kbsoftware")


@pytest.mark.django_db
def test_host_name_for_url_no_protocol():
    assert "www.kbsoftware.co.uk" == hostname_for_url("www.kbsoftware.co.uk")


@pytest.mark.django_db
def test_host_name_for_url_no_protocol_or_subdomain():
    assert "kbsoftware.co.uk" == hostname_for_url("kbsoftware.co.uk")


@pytest.mark.django_db
def test_prepend_url_with_http():
    assert "http://www.kbsoftware.co.uk" == prepend_url_with_http(
        "www.kbsoftware.co.uk"
    )


@pytest.mark.django_db
def test_prepend_url_with_http_https():
    assert "https://www.kbsoftware.co.uk" == prepend_url_with_http(
        "https://www.kbsoftware.co.uk"
    )


@pytest.mark.django_db
def test_prepend_url_with_http_no_change():
    assert "http://www.kbsoftware.co.uk" == prepend_url_with_http(
        "http://www.kbsoftware.co.uk"
    )
