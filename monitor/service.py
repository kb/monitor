# -*- encoding: utf-8 -*-
import attr
import json
import requests

from django.conf import settings
from http import HTTPStatus
from rich.pretty import pprint


@attr.s
class Droplet:
    droplet_id = attr.ib()
    minion = attr.ib()
    memory = attr.ib()
    disk = attr.ib()
    price_monthly = attr.ib()
    tags = attr.ib()
    domains = attr.ib()


@attr.s
class Volume:
    name = attr.ib()
    size = attr.ib()
    droplets = attr.ib()
    tags = attr.ib()


class CloudSpace:
    def __init__(self):
        self.api_url_base = "https://my.cloudspaceuk.co.uk/api/"
        self.headers = {
            "Content-Type": "application/json",
            # "Authorization": "Basic {}".format(self.api_token),
        }

    def _auth(self):
        return (settings.CLOUDSPACE_USERNAME, settings.CLOUDSPACE_PASSWORD)

    def servers(self):
        result = []
        api_url = "{}details".format(self.api_url_base)
        print()
        print()
        print(api_url)
        print(api_url)
        print(api_url)
        print()
        print()

        # from requests.auth import HTTPBasicAuth
        # auth = HTTPBasicAuth(
        #    settings.CLOUDSPACE_USERNAME, settings.CLOUDSPACE_PASSWORD
        # )
        ## r = requests.post(url, auth=auth)
        # req = requests.get(
        #    "https://my.cloudspaceuk.co.uk/api/details", auth=auth
        # )

        # req = requests.get(
        #     "https://my.cloudspaceuk.co.uk/api/details",
        #     auth=(settings.CLOUDSPACE_USERNAME, settings.CLOUDSPACE_PASSWORD),
        # )
        # , headers=self.headers)

        response = requests.get(
            "https://my.cloudspaceuk.co.uk/api/details",
            auth=(settings.CLOUDSPACE_USERNAME, settings.CLOUDSPACE_PASSWORD),
        )

        # response = requests.get(api_url, auth=self._auth())
        pprint(response, expand_all=True)
        # pprint(response.json(), expand_all=True)
        return result


class DigitalOcean:
    def __init__(self):
        """Digital Ocean API.

        From, How To Use Web APIs in Python 3
        https://www.digitalocean.com/community/tutorials/how-to-use-web-apis-in-python-#!/usr/bin/env python3

        """
        self.api_token = settings.DIGITAL_OCEAN_TOKEN
        self.api_url_base = "https://api.digitalocean.com/v2/"
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(self.api_token),
        }

    def servers(self):
        result = []
        api_url = "{}droplets".format(self.api_url_base)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == HTTPStatus.OK:
            data = json.loads(response.content.decode("utf-8"))
            droplets = data["droplets"]
            for droplet in droplets:
                minion = droplet["name"]
                size = droplet["size"]
                # 06/05/2022, The project does not appear in the Droplet data, so
                # use the tags for now...
                tags = droplet["tags"]
                result.append(
                    Droplet(
                        droplet_id=droplet["id"],
                        minion=minion,
                        memory=droplet["memory"],
                        disk=droplet["disk"],
                        price_monthly=size["price_monthly"],
                        tags=[x for x in tags],
                        domains=[],
                    )
                )
        else:
            pprint(response, expand_all=True)
            pprint(response.json(), expand_all=True)
            raise Exception(
                "Error from the Digital Ocean API: {}".format(
                    response.status_code
                )
            )
        return result
        # return dict(sorted(result.items()))

    def volumes(self):
        """

        09/08/2022, email from support@digitalocean.com

          I understand you are inquiring about the possibility of checking the
          cost of a volume via an API call. Please note that it's not possible
          at the moment as we don't have the pricing endpoints for the API.

        https://www.kbsoftware.co.uk/crm/ticket/5774/

        """
        result = []
        api_url = "{}volumes".format(self.api_url_base)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == HTTPStatus.OK:
            data = json.loads(response.content.decode("utf-8"))
            volumes = data["volumes"]
            for volume in volumes:
                pprint(volume, expand_all=True)
                name = volume["name"]
                size = volume["size_gigabytes"]
                droplet_ids = volume["droplet_ids"]
                tags = volume["tags"]
                result.append(
                    Volume(
                        name=name,
                        size=size,
                        droplets=[x for x in droplet_ids],
                        tags=[x for x in tags],
                    )
                )
        else:
            pprint(response, expand_all=True)
            raise Exception(
                "Error from the Digital Ocean API: {}".format(
                    response.status_code
                )
            )
        return result


class Linode:
    def __init__(self):
        self.api_token = settings.LINODE_TOKEN
        self.api_url_base = "https://api.linode.com/v4/linode/"
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {0}".format(self.api_token),
        }
        self.linode_types = {}

    def _get_linode_type(self, linode_type):
        """

        https://www.linode.com/docs/api/linode-types/#type-view

        """
        if linode_type in self.linode_types:
            pass
        else:
            api_url = "{}types/{}".format(self.api_url_base, linode_type)
            response = requests.get(api_url, headers=self.headers)
            if response.status_code == HTTPStatus.OK:
                data = json.loads(response.content.decode("utf-8"))
                self.linode_types["linode_type"] = data
            else:
                pprint(response, expand_all=True)
                raise Exception(
                    f"Error from the Linode types API: {response.status_code}"
                )
        return self.linode_types["linode_type"]

    def servers(self):
        """

        From, Create a Linode Using the Linode API
        https://www.linode.com/docs/guides/getting-started-with-the-linode-api/

        """
        result = []
        api_url = "{}instances".format(self.api_url_base)
        response = requests.get(api_url, headers=self.headers)
        if response.status_code == HTTPStatus.OK:
            data = json.loads(response.content.decode("utf-8"))
            # pprint(data, expand_all=True)
            droplets = data["data"]
            for droplet in droplets:
                minion = droplet["label"]
                linode_type = self._get_linode_type(droplet["type"])
                # size = droplet["size"]
                tags = droplet["tags"]
                specs = droplet["specs"]
                result.append(
                    Droplet(
                        droplet_id=droplet["id"],
                        minion=minion,
                        memory=specs["memory"],
                        disk=specs["disk"],
                        price_monthly=linode_type["price"]["monthly"],
                        tags=tags,
                        domains=[],
                    )
                )
        else:
            pprint(response, expand_all=True)
            pprint(response.json(), expand_all=True)
            raise Exception(
                "Error from the Linode API: {}".format(response.status_code)
            )
        return result
