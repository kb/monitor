# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    ContactDomainCreateView,
    ContactDomainDeleteView,
    ContactDomainUpdateView,
    MonitorListView,
    MonitorReportListView,
)


urlpatterns = [
    re_path(r"^$", view=MonitorListView.as_view(), name="monitor.list"),
    re_path(
        r"^contact/(?P<pk>\d+)/domain/create/$",
        view=ContactDomainCreateView.as_view(),
        name="monitor.contact.domain.create",
    ),
    re_path(
        r"^contact/domain/(?P<pk>\d+)/delete/$",
        view=ContactDomainDeleteView.as_view(),
        name="monitor.contact.domain.delete",
    ),
    re_path(
        r"^contact/domain/(?P<pk>\d+)/update/$",
        view=ContactDomainUpdateView.as_view(),
        name="monitor.contact.domain.update",
    ),
    re_path(
        r"^(?P<slug>[-\w]+)/$",
        view=MonitorReportListView.as_view(),
        name="monitor.report.list",
    ),
]
