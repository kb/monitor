# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.contrib.auth import get_user_model
from django.conf import settings

from login.models import SYSTEM_GENERATED
from monitor.models import exception_notify_by_email, Monitor


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def check_domain_name():
    """Check the expiry date for the domain name and certificate."""
    count = 0
    logger.info("'monitor.tasks.check_domain_name'...")
    user = get_user_model().objects.get(username=SYSTEM_GENERATED)
    try:
        monitor = Monitor.objects.get(slug=Monitor.DOMAIN_NAME)
        count = monitor.monitor()
    except Exception as e:
        logger.exception(e)
        exception_notify_by_email("monitor.tasks.check_domain_name", e, user)
        raise
    logger.info(
        f"'monitor.tasks.check_domain_name' - {count} records - complete"
    )
    return count


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def check_sparkpost():
    count = 0
    logger.info("'monitor.tasks.check_sparkpost'...")
    user = get_user_model().objects.get(username=SYSTEM_GENERATED)
    try:
        monitor = Monitor.objects.get(slug=Monitor.SPARKPOST)
        count = monitor.monitor()
    except Exception as e:
        logger.exception(e)
        exception_notify_by_email("monitor.tasks.check_sparkpost", e, user)
        raise
    logger.info(f"'monitor.tasks.check_sparkpost' - {count} records - complete")
    return count
