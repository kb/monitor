# -*- encoding: utf-8 -*-
from django import forms

from .models import DomainName


class DomainNameEmptyForm(forms.ModelForm):
    class Meta:
        model = DomainName
        fields = ()


class DomainNameForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["url"]
        f.widget.attrs.update({"class": "pure-input-1"})
        f.label = "Domain Name"

    class Meta:
        model = DomainName
        fields = ("url",)
