# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, ListView, UpdateView

from base.view_utils import BaseMixin
from .forms import DomainNameEmptyForm, DomainNameForm
from .models import DomainName, get_contact_model, Monitor


class ContactDomainCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    CreateView,
):
    form_class = DomainNameForm
    model = DomainName

    def _contact(self):
        pk = self.kwargs.get("pk")
        return get_contact_model().objects.get(pk=pk)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.contact = self._contact()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.contact.pk])


class ContactDomainDeleteView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
):
    form_class = DomainNameEmptyForm
    model = DomainName

    template_name = "monitor/contact_domain_delete_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_deleted(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.contact.pk])


class ContactDomainUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    BaseMixin,
    UpdateView,
):
    form_class = DomainNameForm
    model = DomainName

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self.object.contact))
        return context

    def get_success_url(self):
        return reverse("contact.detail", args=[self.object.contact.pk])


class MonitorListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return Monitor.objects.current()


class MonitorReportListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """List of monitor reports e.g. domain names.

    .. note:: The template will be determined by the model class returned from
              ``get_queryset`` e.g. for ``DomainName`` the template used will
              be ``domainname_list.html``.

    """

    paginate_by = 20

    def _monitor(self):
        slug = self.kwargs["slug"]
        monitor = Monitor.objects.get(slug=slug)
        return monitor

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(monitor=self._monitor()))
        return context

    def get_queryset(self):
        monitor = self._monitor()
        return monitor.report()
