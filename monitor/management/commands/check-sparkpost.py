# -*- encoding: utf-8 -*-
import whois

from django.core.management.base import BaseCommand
from rich.pretty import pprint

from monitor.models import SparkPost
from monitor.tasks import check_sparkpost


class Command(BaseCommand):

    help = "Check SparkPost Metrics"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        check_sparkpost()
        # display the most recent data...
        result = {}
        for x in SparkPost.objects.current():
            result[x.domain] = {
                "count_sent": x.count_sent,
                "count_bounce": x.count_bounce,
            }
        pprint(result, expand_all=True)
        self.stdout.write("Complete: {}".format(self.help))
