# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from monitor.models import DomainName


class Command(BaseCommand):

    help = "Costings - domain configuration from Salt pillar #5774"

    def handle(self, *args, **options):
        """Parse the Salt pillar to get the domain configuration.

        Documentation:
        https://www.kbsoftware.co.uk/docs/app-monitor.html

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/5774/

        """
        self.stdout.write(self.help)
        domain_config = DomainName.objects.salt_get_domain_config()
        # sort the data
        domain_config = dict(sorted(domain_config.items()))
        file_name = DomainName.objects.dump_domain_config(domain_config)
        self.stdout.write("\nDump file:\n{}\n\n".format(file_name))
        self.stdout.write("Complete: {}".format(self.help))
