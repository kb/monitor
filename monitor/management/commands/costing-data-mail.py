# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand, CommandError
from rich.pretty import pprint

from monitor.models import DomainName, SparkPost
from monitor.tasks import check_sparkpost


class Command(BaseCommand):

    help = "Costings - email configuration from SparkPost #5774"

    def handle(self, *args, **options):
        """Call the SparkPost API to get mail information.

        Documentation:
        https://www.kbsoftware.co.uk/docs/app-monitor.html

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/5774/

        """
        self.stdout.write(self.help)
        # domain data (from the Salt pillars)
        # salt_domains = self._salt_domain_data()
        check_sparkpost()
        # display the most recent data...
        result = {}
        for x in SparkPost.objects.current():
            result[x.domain] = x.count_sent
        pprint(result, expand_all=True)
        for domain, data in result.items():
            qs = DomainName.objects.for_url(domain)
            if qs.exists():
                pass
            else:
                raise CommandError(
                    "SparkPost is sending email for {}, "
                    "but the domain name is not linked to "
                    "a contact in the database".format(domain)
                )
        file_name = "costing-data-mail.json"
        with open(file_name, "w") as f:
            json.dump(result, f, indent=4)
        self.stdout.write("\nDump file:\n{}\n\n".format(file_name))
        self.stdout.write("Complete: {}".format(self.help))
