# -*- encoding: utf-8 -*-
import attr
import json

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from monitor.service import DigitalOcean, Linode


class Command(BaseCommand):

    help = "Costings - server configuration from hosting providers #5774"

    def _json_dump_droplets(self, droplets):
        data = []
        file_name = "costing-data-server.json"
        for droplet in droplets:
            data.append(attr.asdict(droplet))
        with open(file_name, "w") as f:
            json.dump(data, f, indent=4)
        # rprint("[yellow]2. 'json_dump_droplets' to '{}'...".format(file_name))
        return file_name

    def handle(self, *args, **options):
        """Call the APIs of our hosting companies to get server information.

        1. Run ``django-admin costing-data-domain``
           (create the ``costing-data-domain.json`` dump file).
        2. Read the domain configuration from the dump file.
        3. Call the Digital Ocean, Linode and CloudSpace APIs.
        4. Combine the hosting information with the domain configuration.
        5. Write the output to the ``costing-data-server.json' file.

        Documentation:
        https://www.kbsoftware.co.uk/docs/app-monitor.html

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/5774/

        """
        self.stdout.write(self.help)

        # volumes = DigitalOcean().volumes()
        # pprint(volumes, expand_all=True)
        # return

        file_name = "costing-data-domain.json"
        try:
            with open(file_name, "r") as f:
                domain_config = json.load(f)
        except FileNotFoundError:
            raise CommandError(
                "Cannot open '{}'. "
                "Did you run the '{}' management command first?".format(
                    file_name, "costing-data-domain"
                )
            )
        # find the domain names for each minion (server)
        minions = {}
        for domain_name, config in domain_config.items():
            minion_id = "{}@{}".format(config["pillar"], config["minion"])
            if not minion_id in minions:
                minions[minion_id] = []
            minions[minion_id].append(domain_name)
        # pprint(minions, expand_all=True)
        # get a list of cloud servers (droplets)
        droplets = []
        # droplets = droplets + CloudSpace().servers()
        # pprint(droplets, expand_all=True)
        # return
        droplets = droplets + DigitalOcean().servers()
        droplets = droplets + Linode().servers()
        # link the domain names to the droplets
        for droplet in droplets:
            minion_id = "{}@{}".format(
                settings.MONITOR_COSTINGS_DEFAULT_PILLAR, droplet.minion
            )
            if minion_id in minions:
                droplet.domains = minions.pop(minion_id)
        # pprint(droplets, expand_all=True)
        file_name = self._json_dump_droplets(droplets)
        self.stdout.write("\nDump file:\n{}\n\n".format(file_name))
        self.stdout.write("Complete: {}".format(self.help))
