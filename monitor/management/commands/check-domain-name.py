# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from monitor.tasks import check_domain_name


class Command(BaseCommand):

    help = "Check domain names for expiry"

    def handle(self, *args, **options):
        """Check domain names for expiry.

        Code copied from:

        - https://github.com/averi/python-scripts/blob/master/check-domain-expiration.py
        - https://github.com/codebox/https-certificate-expiry-checker

        """
        self.stdout.write(self.help)
        check_domain_name()
        self.stdout.write("Complete: {}".format(self.help))
