# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from login.models import SYSTEM_GENERATED
from monitor.models import Monitor


class Command(BaseCommand):

    help = "Initialise 'monitor' application"

    def _check_domain_name(self):
        monitor = Monitor.objects.init_monitor(
            Monitor.DOMAIN_NAME,
            "Domain Name Expiry",
            24,
            0,
            "monitor",
            "models",
            "DomainName",
        )
        self.stdout.write(f"Monitor: {monitor.title} ({monitor.slug})")

    def _check_sparkpost(self):
        monitor = Monitor.objects.init_monitor(
            Monitor.SPARKPOST,
            "SparkPost Bounce",
            1,
            0,
            "monitor",
            "models",
            "SparkPost",
        )
        self.stdout.write(f"Monitor: {monitor.title} ({monitor.slug})")

    def _system_generated_user(self):
        user_model = get_user_model()
        try:
            user_model.objects.get(username=SYSTEM_GENERATED)
        except user_model.DoesNotExist:
            raise CommandError(
                f"User '{SYSTEM_GENERATED}' does not exist.  Scheduled tasks for 'monitor' will fail."
            )

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        self._check_domain_name()
        self._check_sparkpost()
        self._system_generated_user()
