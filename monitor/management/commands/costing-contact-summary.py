# -*- encoding: utf-8 -*-
import attr
import csv
import json
import operator
import urllib.parse

from decimal import Decimal
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from rich import print as rprint
from rich.pretty import pprint

from monitor.models import DomainName, get_contact_model


@attr.s
class Contact:
    contact_name = attr.ib()
    contact_url = attr.ib()
    domains = attr.ib()
    servers = attr.ib()
    total = attr.ib()


@attr.s
class Domain:
    url = attr.ib()
    cost_backup = attr.ib()
    cost_mail = attr.ib()
    cost_server = attr.ib()


@attr.s
class Server:
    minion = attr.ib()
    cost = attr.ib()


class Command(BaseCommand):

    help = "Domain config (costs) #5774"

    def add_arguments(self, parser):
        parser.add_argument("monthly_cost_backup", type=int)
        parser.add_argument("monthly_cost_mail", type=int)

    def _check_for_missing_domain(self, contacts, url):
        found = False
        for user_name, contact in contacts.items():
            for domain in contact.domains:
                if url in domain.url:
                    found = True
                    break
            if found:
                break
        if not found:
            qs = DomainName.objects.for_url(url)
            if qs.exists():
                for domain_name in qs:
                    user_name = self._get_contact(
                        domain_name.contact.user.username, contacts
                    )
                    contacts[user_name].domains.append(
                        Domain(
                            url=domain_name.url,
                            cost_backup=Decimal(),
                            cost_mail=Decimal(),
                            cost_server=Decimal(),
                        )
                    )
            else:
                raise CommandError(
                    f"Cannot find a contact for domain name '{url}'"
                )

    def _costing_data_backup(self, contacts, monthly_cost):
        with open("costing-data-backup.json", "r") as f:
            backup_data = json.load(f)
        self._costing_data_backup_domains(backup_data, contacts)
        total = Decimal()
        for url, gb in backup_data.items():
            total = total + Decimal(gb)
        for url, gb in backup_data.items():
            found = False
            for user_name, contact in contacts.items():
                for domain in contact.domains:
                    if url in domain.url:
                        domain.cost_backup = monthly_cost * (
                            (Decimal(gb) / total)
                        ).quantize(Decimal(".001"))
                        found = True
                        break
                if found:
                    break
            if not found:
                raise CommandError(
                    f"Cannot find '{url}' domain for backup costings"
                )

    def _costing_data_backup_domains(self, backup_data, contacts):
        """Add missing domains to 'contacts'."""
        print()
        for url, gb in backup_data.items():
            self._check_for_missing_domain(contacts, url)

    def _costing_data_mail(self, contacts, monthly_cost):
        print()
        with open("costing-data-mail.json", "r") as f:
            mail_data = json.load(f)
        total_count = 0
        for mail_domain, mail_count in mail_data.items():
            total_count = total_count + mail_count
            self._check_for_missing_domain(contacts, mail_domain)
        for mail_domain, mail_count in mail_data.items():
            found = False
            for user_name, contact in contacts.items():
                for domain in contact.domains:
                    if mail_domain in domain.url:
                        domain.cost_mail = monthly_cost * (
                            Decimal(mail_count / total_count)
                        ).quantize(Decimal(".01"))
                        found = True
                        break
                if found:
                    break
            if not found:
                raise CommandError(
                    f"Cannot find '{mail_domain}' domain for mail costings"
                )

    def _costing_data_server(self):
        contacts = {}
        with open("costing-data-server.json", "r") as f:
            servers = json.load(f)
        for server in servers:
            domains = server["domains"]
            minion = server["minion"]
            number_of_sites = len(domains)
            price_monthly = Decimal(server["price_monthly"] or "0")
            if not price_monthly:
                raise CommandError(
                    f"Server '{minion}' has a zero monthly cost."
                )
            if number_of_sites:
                cost_backup = cost_mail = cost_server = Decimal("0.00")
                if number_of_sites and price_monthly:
                    cost_server = (price_monthly / number_of_sites).quantize(
                        Decimal(".01")
                    )
                for url in domains:
                    # link the domain name to the contact (or contacts)
                    qs = DomainName.objects.for_url(url)
                    if qs.exists():
                        for domain_name in qs:
                            user_name = self._get_contact(
                                domain_name.contact.user.username, contacts
                            )
                            contacts[user_name].domains.append(
                                Domain(
                                    url=domain_name.url,
                                    cost_backup=cost_backup,
                                    cost_mail=cost_mail,
                                    cost_server=cost_server,
                                )
                            )
                    else:
                        raise CommandError(
                            f"Cannot find a contact for domain name '{url}'"
                        )
            else:
                tags = server["tags"]
                number_of_tags = len(tags)
                if number_of_tags == 0:
                    raise CommandError(
                        f"Server '{minion}' has no domains and no tags. "
                        "Who should pay for it?"
                    )
                elif number_of_tags == 1:
                    for tag in tags:
                        user_name = self._get_contact(tag, contacts)
                        contacts[user_name].servers.append(
                            Server(minion=minion, cost=price_monthly)
                        )

                else:
                    raise CommandError(
                        f"Server '{minion}' has no domains and several tags. "
                        "Who should pay for it?"
                    )
        return contacts

    def _get_contact(self, user_name, contacts):
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user__username=user_name)
            user_name = contact.user.username
            if not user_name in contacts:
                absolute_url = contact.get_absolute_url()
                contact_url = urllib.parse.urljoin(
                    settings.HOST_NAME, absolute_url
                )
                contacts[user_name] = Contact(
                    contact_name=contact.get_full_name(),
                    contact_url=contact_url,
                    domains=[],
                    servers=[],
                    total=Decimal(),
                )
            return user_name
        except contact_model.DoesNotExist:
            raise CommandError(f"Cannot find a contact for tag '{user_name}'")

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        monthly_cost_backup = Decimal(options["monthly_cost_backup"])
        monthly_cost_mail = Decimal(options["monthly_cost_mail"])
        contacts = self._costing_data_server()
        self._costing_data_backup(contacts, monthly_cost_backup)
        self._costing_data_mail(contacts, monthly_cost_mail)
        # calculate total costs
        for user_name, costs in contacts.items():
            total = Decimal()
            domains = costs.domains
            for domain in domains:
                total = (
                    total
                    + domain.cost_backup
                    + domain.cost_server
                    + domain.cost_mail
                )
            servers = costs.servers
            for server in servers:
                total = total + server.cost
            costs.total = total
        pprint(contacts, expand_all=True)

        # create the CSV report
        csv_file_name = "costing-contact-summary.csv"
        with open(csv_file_name, "w", newline="") as csv_file:
            csv_writer = csv.writer(
                csv_file,
                dialect="excel-tab",
            )
            csv_writer.writerow(["Name", "Product Type", "Cost"])
            for user_name, costs in dict(sorted(contacts.items())).items():
                csv_writer.writerow(["", "", ""])
                csv_writer.writerow(
                    ["{} ({})".format(costs.contact_name, user_name), ""]
                )
                domains = costs.domains
                # domains.sort(key=operator.itemgetter("contact_url"))
                for domain in domains:
                    if domain.cost_server:
                        csv_writer.writerow(
                            [domain.url, "server", domain.cost_server]
                        )
                    if domain.cost_backup:
                        csv_writer.writerow(
                            [domain.url, "backup", domain.cost_backup]
                        )
                    if domain.cost_mail:
                        csv_writer.writerow(
                            [domain.url, "mail", domain.cost_mail]
                        )
                servers = costs.servers
                # servers.sort(key=operator.itemgetter("minion"))
                for server in servers:
                    csv_writer.writerow([server.minion, "server", server.cost])
                csv_writer.writerow(["Total", "", costs.total])
        rprint("[cyan]Costs by contact written to: '{}'".format(csv_file_name))
        self.stdout.write("{} - Complete".format(self.help))
