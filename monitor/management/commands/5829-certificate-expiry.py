# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from monitor.models import DomainName


class Command(BaseCommand):

    help = "Check the certificate expiry date for a domain name #5829"

    def add_arguments(self, parser):
        parser.add_argument("url", type=str)

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        found = False
        url = options["url"]
        qs = DomainName.objects.for_url(url)
        for domain_name in qs:
            found = domain_name.is_match(url)
            if found:
                self.stdout.write(
                    f"Certificate for {domain_name.url} "
                    f"expires {domain_name.certificate_expiry_date_time()}"
                )
        if not found:
            raise CommandError(f"Cannot find '{url}' in the 'DomainName' model")
        self.stdout.write(f"{self.help} - Complete")
