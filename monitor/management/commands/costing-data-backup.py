# -*- encoding: utf-8 -*-
import json

from decimal import Decimal, ROUND_UP
from django.core.management.base import BaseCommand, CommandError

from monitor.models import DomainName, hostname_for_url, prepend_url_with_http


class Command(BaseCommand):

    help = "Costings - for backups with rsync.net #5774"

    def _check_domain_names_in_database(self, domain_config):
        """Check domain names from the Salt Pillar are in 'DomainName'.

        Arguments:
        domain_config -- domain information from the Salt Pillar

        """
        missing_domain_names = (
            DomainName.objects.check_domain_names_in_database(
                domain_config.keys(), ignore_invalid_domain_name=True
            )
        )
        if missing_domain_names:
            raise CommandError(
                f"Domain names '{missing_domain_names}' do not exist "
                "in the 'DomainName' model"
            )

    def handle(self, *args, **options):
        """Check rsync.net to find the space used for backups.

        Documentation:
        https://www.kbsoftware.co.uk/docs/app-monitor.html

        1. Run ``django-admin costing-data-domain``
           (create the ``costing-data-domain.json`` dump file).
        2. Read the domain configuration from the dump file.

        Ticket:
        https://www.kbsoftware.co.uk/crm/ticket/5774/

        """
        self.stdout.write(self.help)
        file_name = "costing-data-domain.json"
        try:
            with open(file_name, "r") as f:
                domain_config = json.load(f)
        except FileNotFoundError:
            raise CommandError(
                f"Cannot open '{file_name}'. Did you run the "
                "'costing-data-domain' management command first?"
            )
        self._check_domain_names_in_database(domain_config)
        data = DomainName.objects.backup_ssh()
        for domain, gb in data.items():
            qs = DomainName.objects.for_url(domain)
            if qs.exists():
                pass
            else:
                raise CommandError(
                    f"We have backup data for {domain}, but the domain "
                    "name is not linked to a contact in the database"
                )
        file_name = "costing-data-backup.json"
        with open(file_name, "w") as f:
            json.dump(data, f, indent=4)
        self.stdout.write("\nDump file:\n{}\n\n".format(file_name))
        total = Decimal()
        for _, gb in data.items():
            total = total + Decimal(gb)
        # convert to an integer
        total = total.quantize(Decimal("1."), rounding=ROUND_UP)
        self.stdout.write(f"Total space used, {total}GB. Complete: {self.help}")
