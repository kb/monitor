# -*- encoding: utf-8 -*-
import fnmatch
import importlib
import json
import logging
import pathlib
import paramiko
import pytz
import requests
import socket
import ssl
import whois
import yaml

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.db import models
from django.db import transaction
from django.utils import timezone
from http import HTTPStatus
from paramiko.ssh_exception import AuthenticationException
from reversion import revisions as reversion
from urllib.parse import urlparse

from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel
from mail.models import Notify
from mail.service import queue_mail_message


logger = logging.getLogger(__name__)


def exception_notify_by_email(decscription, e, system_user):
    email_addresses = [n.email for n in Notify.objects.all()]
    if email_addresses:
        queue_mail_message(
            system_user,
            email_addresses,
            f"Error '{decscription}'",
            f"Cannot '{decscription}': {e}",
        )
    else:
        logger.error(
            "Cannot send email notification.  "
            "No email addresses set-up in 'mail.models.Notify'"
        )


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


def hostname_for_url(url):
    url_with_http = prepend_url_with_http(url)
    x = urlparse(url_with_http)
    return x.hostname


def prepend_url_with_http(url):
    result = url
    if not result.startswith("http"):
        result = "http://{}".format(result)
    return result


class MonitorError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class MonitorManager(models.Manager):
    def create_monitor(
        self, slug, title, hours, minutes, app, module, report_class
    ):
        monitor = self.model(
            slug=slug,
            title=title,
            hours=hours,
            minutes=minutes,
            app=app,
            module=module,
            report_class=report_class,
        )
        monitor.save()
        return monitor

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_monitor(
        self, slug, title, hours, minutes, app, module, report_class
    ):
        try:
            x = self.model.objects.get(slug=slug)
            x.title = title
            x.hours = hours
            x.minutes = minutes
            x.app = app
            x.module = module
            x.report_class = report_class
            x.save()
        except self.model.DoesNotExist:
            x = self.create_monitor(
                slug, title, hours, minutes, app, module, report_class
            )
        return x


class Monitor(TimedCreateModifyDeleteModel):
    """Header records for monitoring."""

    DOMAIN_NAME = "domain_name"
    SPARKPOST = "sparkpost"

    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=200)
    hours = models.IntegerField()
    minutes = models.IntegerField()
    checked = models.DateTimeField(blank=True, null=True)
    app = models.CharField(max_length=100)
    module = models.CharField(max_length=100)
    report_class = models.CharField(max_length=100)
    objects = MonitorManager()

    class Meta:
        ordering = ("slug",)
        verbose_name = "Monitor"
        verbose_name_plural = "Monitors"

    def __str__(self):
        return "{} ('{}')".format(self.title, self.slug)

    def _report_class(self):
        """Get the report class for this monitor e.g. 'DomainName'."""
        class_name = "{}.{}".format(self.app, self.module)
        # import the report module
        report_module = importlib.import_module(class_name)
        try:
            # get the class
            return getattr(report_module, self.report_class)
        except AttributeError as e:
            logger.error(e)
            raise MonitorError(e)

    def due(self):
        result = None
        if self.checked:
            result = self.checked + relativedelta(
                hours=+self.hours, minutes=+self.minutes
            )
        return result

    def monitor(self):
        result = 0
        report_class = self._report_class()
        with transaction.atomic():
            try:
                report_class.objects.initialise()
                qs = report_class.objects.current()
            except AttributeError as e:
                logger.error(e)
                raise MonitorError(e)
            pks = [x.pk for x in qs]
            logger.debug("Monitor: '{}' - {} rows".format(len(pks), self.slug))
            for pk in pks:
                report = report_class.objects.get(pk=pk)
                report.monitor()
                result = result + 1
            self.checked = timezone.now()
            self.save()
        logger.debug("Monitor: '{}' - complete".format(len(pks), self.slug))
        return result

    def report(self):
        """Return a list of monitor reports e.g. domain names."""
        report_class = self._report_class()
        return report_class.objects.current().order_by("certificate_expiry")

    def warning(self):
        """If the monitor is overdue, then warn the user."""
        result = None
        if self.checked:
            due = self.due()
            if timezone.now() > due:
                result = "is overdue"
        else:
            result = "has never been checked"
        return result


class DomainNameManager(models.Manager):
    def _create_domain_name(self, contact, url):
        x = self.model(contact=contact, url=url)
        x.save()
        return x

    def _salt_get_domain_names(self, pillar_folder, wildcard, pillar):
        """Find the server configuration for each site / domain name.

        .. warning:: We add the configuration for the site / domain name after
                     merging the configuration for the server
                     (including *wildcard* data).
                     Salt probably resolves this in a different order!

        """
        result = {}
        with open(pathlib.Path(pillar_folder, "top.sls")) as f:
            # load the 'top.sls' file
            data = yaml.safe_load(f)
            base = data["base"]
            for host_name, base_config in base.items():
                if "*" in host_name:
                    # exclude wildcard e.g. 'pc-*' (see '_salt_get_wildcard')
                    pass
                else:
                    server_config = {}
                    for include in base_config:
                        if isinstance(include, str):
                            # convert include to file and path
                            # e.g. 'sites.cw-3' to 'sites/cw-3.sls'
                            path_file = include.split(".")
                            path_file[-1] = path_file[-1] + ".sls"
                            with open(
                                pathlib.Path(pillar_folder, *path_file)
                            ) as f:
                                server_config.update(yaml.safe_load(f))
                    if "sites" in server_config:
                        sites = server_config.pop("sites")
                        for domain_name, domain_config in sites.items():
                            # config for domain name (site)
                            result[domain_name] = {}
                            # merge in the *wildcard* config
                            result[domain_name].update(
                                self._salt_merge_wildcard(host_name, wildcard)
                            )
                            # merge in the config for this server
                            result[domain_name].update(server_config)
                            # add the server (host) name
                            result[domain_name].update({"minion": host_name})
                            # add the pillar ID e.g. 'kb'
                            result[domain_name].update({"pillar": pillar})
                            # finally - merge the domain config
                            result[domain_name].update(domain_config)
        return result

    def _salt_get_wildcard(self, pillar_folder):
        """Get the config for the wildcard include files.

        The ``top.sls`` file for our pillar, includes wildcards e.g::

          '*':
            - global.users
          'kb-* and not kb-vpn':
            - config.django
            - config.monitor
            - config.nginx

        This method retrieves the *wildcard* config, so it can be merged into the
        site later on (using ``_salt_match_minion``).

        """
        result = {}
        with open(pathlib.Path(pillar_folder, "top.sls")) as f:
            # load the 'top.sls' file
            data = yaml.safe_load(f)
            base = data["base"]
            for host_name, config in base.items():
                if "*" in host_name:
                    result[host_name] = {}
                    for include in config:
                        if isinstance(include, str):
                            # not sure if / why we need to do this...
                            if include.startswith("sites"):
                                pass
                            else:
                                # convert include to file and path
                                # e.g. 'config.monitor' to 'config/monitor.sls'
                                path_file = include.split(".")
                                path_file[-1] = path_file[-1] + ".sls"
                                with open(
                                    pathlib.Path(pillar_folder, *path_file)
                                ) as f:
                                    include_config = yaml.safe_load(f)
                                    result[host_name].update(include_config)
        return result

    def _salt_match_minion(self, minion_id, salt_top):
        """Copied from ``_match`` (see ``lib/pillarinfo.py`` in ``fabric``."""
        result = False
        if minion_id is None:
            result = True
        else:
            # 20/04/2021, Try and handle ``and not``
            # (when I don't really understand parsing)
            # e.g. 'drop-* and not drop-vpn'
            tokens = salt_top.split(" ")
            index_and = index_not = minion_not = None
            try:
                index_and = tokens.index("and")
                index_not = tokens.index("not")
            except ValueError:
                pass
            # if minion_id == "kb-a" and index_and and index_not:
            # import pdb; pdb.set_trace()
            if (
                index_and
                and index_not
                and index_and > 0
                and (index_and + 1 == index_not)
                and (index_not + 2 == len(tokens))
            ):
                minion_not = tokens[index_not + 1].strip()
                pos = salt_top.find(" and ")
                salt_top = salt_top[:pos].strip()
            # exclude the ``and not`` value e.g. ``drop-vpn``
            if minion_not and minion_not == minion_id:
                pass
            else:
                result = fnmatch.fnmatch(minion_id, salt_top)
                if not result:
                    for item in salt_top.split(","):
                        result = fnmatch.fnmatch(minion_id, item)
                        if result:
                            break
        return result

    def _salt_merge_wildcard(self, host_name, wildcard):
        """Merge configuration from files matching the wildcard.

        e.g. ``nc-*`` matches ``nc-a``, so ``nc-a`` will include the
        configuration data from ``nc-*``.

        """
        result = {}
        for wildcard_host, config in wildcard.items():
            if self._salt_match_minion(host_name, wildcard_host):
                result.update(config)
        return result

    def _backup_ssh_space(self, ssh_client, folders):
        result = {}
        print()
        for folder_name in folders:
            command = "du -Ahd0 {}".format(
                pathlib.Path(settings.BACKUP_SSH_FOLDER, folder_name)
            )
            (stdin, stdout, stderr) = ssh_client.exec_command(command)
            for line in stdout.readlines():
                line = line.strip()
                pos = -1
                usage = None
                if pos == -1:
                    pos = line.find("G")
                    if pos > 0:
                        usage = Decimal(line[:pos])
                if pos == -1:
                    pos = line.find("M")
                    if pos > 0:
                        usage = Decimal(line[:pos]) / Decimal(1000)
                if pos == -1:
                    pos = line.find("K")
                    if pos > 0:
                        usage = (
                            Decimal(line[:pos]) / Decimal(1000) / Decimal(1000)
                        )
                if pos == -1:
                    raise MonitorError(
                        f"The disk usage for folder name '{folder_name}' "
                        f"does not appear to be in KB, MB or GB: '{line}'"
                    )
                else:
                    print(f"{folder_name:<40} {usage:>10.2f}")
                    result[folder_name] = str(usage)
        return result

    def _backup_ssh_folders(self, ssh_client):
        result = []
        command = f"ls -a {settings.BACKUP_SSH_FOLDER}"
        (stdin, stdout, stderr) = ssh_client.exec_command(command)
        for line in stdout.readlines():
            domain_name = line.strip()
            if domain_name:
                if domain_name == "." or domain_name == "..":
                    pass
                else:
                    result.append(domain_name)
        return sorted(result)

    def _is_valid_domain_name(self, url):
        """If the domain name ('url') is valid, it should be in 'DomainName'."""
        result = False
        validate_url = URLValidator()
        try:
            validate_url(prepend_url_with_http(hostname_for_url(url)))
            result = True
        except ValidationError:
            pass
        return result

    def backup_ssh(self):
        """Connect to our rsync.net server and calculate usage."""
        ssh_client = paramiko.SSHClient()
        ssh_client.load_system_host_keys()
        try:
            ssh_client.connect(
                settings.BACKUP_SSH_HOSTNAME, 22, settings.BACKUP_SSH_USER, None
            )
        except AuthenticationException as e:
            logger.error(
                "Cannot connect to ssh server "
                f"{settings.BACKUP_SSH_HOSTNAME}, port 22, "
                f"user {settings.BACKUP_SSH_USER}, {e}"
            )
            raise
        folders = self._backup_ssh_folders(ssh_client)
        result = self._backup_ssh_space(ssh_client, folders)
        ssh_client.close()
        return result

    def check_domain_names_in_database(
        self, domain_names, *, ignore_invalid_domain_name=None
    ):
        """Check this model and make sure it includes the domain names.

        Keyword arguments:
        domain_names -- A list of domain names to check
        ignore_invalid_domain_name -- ignore invalid domain name (default False)

        Returns a list of missing domain names.

        """
        result = []
        if ignore_invalid_domain_name is None:
            ignore_invalid_domain_name = False
        for domain_name in domain_names:
            found = self.has_domain_name(
                domain_name,
                ignore_invalid_domain_name=ignore_invalid_domain_name,
            )
            if not found:
                result.append(domain_name)
        return result

    def current(self, contact=None):
        qs = self.model.objects.exclude(deleted=True)
        if contact:
            qs = qs.filter(contact=contact)
        return qs

    def dump_domain_config(self, domains, file_name=None):
        """Write the domain configuration server from Salt to a file.

        Sample dump data::

          {
              "chat.hatherleigh.info": {
                  "minion": "yb-j",
                  "pillar": "yb"
              },
              "flow.hatherleigh.info": {
                  "minion": "yb-a",
                  "pillar": "yb"
              },
          }

        .. note:: We write only required data to ``domains.json``

        """
        if file_name is None:
            file_name = "costing-data-domain.json"
        data = {}
        for domain_name, config in domains.items():
            data.update(
                {
                    domain_name: {
                        "minion": config["minion"],
                        "pillar": config["pillar"],
                    }
                }
            )
        with open(file_name, "w") as f:
            json.dump(data, f, indent=4)
        return file_name

    def for_url(self, url):
        hostname = hostname_for_url(url)
        return self.current().filter(url__contains=hostname)

    def has_domain_name(self, url, *, ignore_invalid_domain_name=None):
        """Check this model and make sure it includes the URL.

        23/11/2024, I am not 100% sure what I am doing here, but I want to
        check this model includes the domain names from the Salt Pillar.

        A few domain names are not valid (e.g. ``flowable``), so I added the
        option to ignore these.

        Used in the ``costing-data-backup`` management command.

        Keyword arguments:
        ignore_invalid_domain_name -- ignore invalid domain name (default False)

        """
        if ignore_invalid_domain_name is None:
            ignore_invalid_domain_name = False
        result = False
        qs = self.for_url(url)
        for domain_name in qs:
            result = domain_name.is_match(url)
            if result:
                break
        if not result:
            if ignore_invalid_domain_name:
                if self._is_valid_domain_name(url):
                    pass
                else:
                    result = True
        return result

    def init_domain_name(self, contact, url):
        try:
            x = self.model.objects.get(contact=contact, url=url)
        except self.model.DoesNotExist:
            x = self._create_domain_name(contact, url)
        return x

    def initialise(self):
        pass

    def salt_get_domain_config(self):
        """Get the config for each site (domain name) from the Salt pillar."""
        result = {}
        pillar_folder = pathlib.Path.home().joinpath("Private", "deploy")
        for folder in pillar_folder.iterdir():
            if folder.is_dir() and folder.name.startswith("pillar-"):
                pos = folder.name.find("-")
                pillar = folder.name[pos + 1 :]
                if not pillar:
                    raise MonitorError(
                        "The pillar folder name ('{}') needs an identifier "
                        "after the '-'".format(folder.name)
                    )
                wildcard = self._salt_get_wildcard(folder)
                result.update(
                    self._salt_get_domain_names(folder, wildcard, pillar)
                )
        return result


class DomainName(TimedCreateModifyDeleteModel):
    """Check the domain names and certificates.  When do they expire?"""

    DEFAULT_HTTPS_PORT = 443
    SOCKET_CONNECTION_TIMEOUT_SECONDS = 10

    contact = models.ForeignKey(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    url = models.URLField()
    found = models.BooleanField(default=False)
    expiry = models.DateField(blank=True, null=True)
    found_certificate = models.BooleanField(default=False)
    certificate_expiry = models.DateField(blank=True, null=True)
    objects = DomainNameManager()

    class Meta:
        ordering = ["found", "expiry", "contact__user__username"]
        verbose_name = "DomainName"
        verbose_name_plural = "DomainNames"

    def __str__(self):
        result = "{} ({})".format(self.url, self.contact.get_full_name())
        if self.expiry:
            result = "{} expires {}".format(
                result, self.expiry.strftime("%d/%m/%Y")
            )
        return result

    def certificate_expires_soon(self):
        result = True
        if self.certificate_expiry:
            days_30 = timezone.now() + relativedelta(days=30)
            if self.certificate_expiry < days_30.date():
                result = True
            else:
                result = False
        return result

    def certificate_expiry_date_time(self):
        """Check the expiry date of the certificate.

        Code copied from:
        https://github.com/codebox/https-certificate-expiry-checker

        """
        context = ssl.create_default_context()
        host = hostname_for_url(self.url)
        try:
            with socket.create_connection(
                (host, self.DEFAULT_HTTPS_PORT),
                self.SOCKET_CONNECTION_TIMEOUT_SECONDS,
            ) as tcp_socket:
                with context.wrap_socket(
                    tcp_socket, server_hostname=host
                ) as ssl_socket:
                    # certificate_info is a dict with lots of information about the certificate
                    certificate_info = ssl_socket.getpeercert()
                    exp_date_text = certificate_info["notAfter"]
                    return datetime.fromtimestamp(
                        ssl.cert_time_to_seconds(exp_date_text), tz=pytz.UTC
                    )
        except ConnectionRefusedError:
            pass
        except socket.gaierror:
            pass
        except ssl.SSLError:
            pass
        except TimeoutError:
            pass

    def expires_soon(self):
        result = True
        if self.expiry:
            days_30 = timezone.now() + relativedelta(days=30)
            if self.expiry < days_30.date():
                result = True
            else:
                result = False
        return result

    def is_match(self, url):
        """Is the URL a match for this domain (including the sub-domain)?"""
        return hostname_for_url(url) == hostname_for_url(self.url)

    def monitor(self):
        # Check domain name expiry
        expiry = None
        try:
            w = whois.whois(self.url)
            if type(w.expiration_date) == list:
                if len(w.expiration_date):
                    expiry = w.expiration_date[0]
            else:
                expiry = w.expiration_date
        except whois.parser.PywhoisError:
            pass
        self.set_expiry(expiry)
        # Check certificate expiry
        certificate_expiry_date = self.certificate_expiry_date_time()
        if certificate_expiry_date:
            self.set_certificate_expiry(certificate_expiry_date)

    def set_certificate_expiry(self, certificate_expiry_date):
        """Set the expiry date and mark the record as found."""
        self.certificate_expiry = certificate_expiry_date
        self.found_certificate = bool(certificate_expiry_date)
        self.save()

    def set_expiry(self, expiry_date):
        """Set the expiry date and mark the record as found."""
        self.expiry = expiry_date
        self.found = bool(expiry_date)
        self.save()


reversion.register(DomainName)


class SparkPostManager(models.Manager):
    def _create_sparkpost(self, domain, count_sent, count_bounce):
        sparkpost = self.model(
            domain=domain, count_sent=count_sent, count_bounce=count_bounce
        )
        sparkpost.save()
        return sparkpost

    def current(self):
        return self.model.objects.all()

    def init_sparkpost(self, domain, count_sent, count_bounce):
        try:
            x = self.model.objects.get(domain=domain)
            x.count_sent = count_sent
            x.count_bounce = count_bounce
            x.save()
        except self.model.DoesNotExist:
            x = self._create_sparkpost(domain, count_sent, count_bounce)
        return x

    def initialise(self):
        """Update each domain with the number of emails sent and bounced.

        Metrics API
        https://developers.sparkpost.com/api/metrics/

        """
        headers = {
            "Authorization": settings.SPARKPOST_API_KEY,
            "Content-Type": "application/json",
        }
        api_url = "https://api.sparkpost.com/api/v1"
        url = "{}/{}".format(api_url, "metrics/deliverability/sending-domain")
        from_date = date.today() + relativedelta(months=-1)
        params = {
            "from": from_date.strftime("%Y-%m-%dT%H:%M"),
            "metrics": "count_bounce,count_sent",
        }
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == HTTPStatus.OK:
            data = response.json()
            results = data["results"]
            self.model.objects.all().delete()
            for row in results:
                domain = row["sending_domain"]
                count_bounce = row["count_bounce"]
                count_sent = row["count_sent"]
                # so we don't get multiple records per domain?
                self.init_sparkpost(domain, count_sent, count_bounce)
        else:
            raise MonitorError(
                f"Status {response.status_code} "
                f"getting SparkPost metrics: {response.text}"
            )


class SparkPost(TimeStampedModel):

    domain = models.CharField(max_length=200, unique=True)
    count_sent = models.IntegerField()
    count_bounce = models.IntegerField()
    objects = SparkPostManager()

    class Meta:
        ordering = ["-count_sent", "-count_bounce", "domain"]
        verbose_name = "SparkPost"
        verbose_name_plural = "SparkPost"

    def __str__(self):
        return "{} sent {} with {} bounces".format(
            self.domain, self.count_sent, self.count_bounce
        )

    def monitor(self):
        pass
