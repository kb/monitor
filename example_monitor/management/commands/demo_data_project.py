# -*- encoding: utf-8 -*-
import json
import requests

from datetime import date
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from contact.models import Contact
from monitor.models import DomainName


class Command(BaseCommand):

    help = "Demo data for 'monitor' app"

    def _create(self, contact, url, expiry):
        found = bool(expiry)
        domain = DomainName(
            contact=contact, url=url, found=found, expiry=expiry
        )
        domain.save()

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        # contact
        u1 = get_user_model().objects.get(username="web")
        c1 = Contact.objects.create_contact(u1)
        u2 = get_user_model().objects.get(username="staff")
        c2 = Contact.objects.create_contact(u2)
        # domain
        self._create(c1, "pkimber.net", None)
        self._create(c1, "hatherleigh.info", date(2017, 12, 29))
        self._create(c2, "kbsoftware.co.uk", date(2017, 11, 29))
        self._create(c1, "creativemarking.co.uk", date(2017, 10, 29))
        self._create(c2, "productiviticloud.com", date(2017, 9, 29))
        self._create(c2, "does.not.exist.code", None)
        self.stdout.write("Complete: {}".format(self.help))
