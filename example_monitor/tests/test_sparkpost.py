# -*- encoding: utf-8 -*-
import pytest

from django.db import IntegrityError
from django.utils import timezone
from httmock import all_requests, HTTMock
from http import HTTPStatus

from login.tests.factories import UserFactory
from monitor.models import SparkPost
from .factories import MonitorFactory, SparkPostFactory


@all_requests
def sparkpost_response(url, request):
    return {
        "status_code": HTTPStatus.OK,
        "content": {
            "results": [
                {
                    "sending_domain": "kbsoftware.co.uk",
                    "count_sent": 57,
                    "count_bounce": 22,
                },
                {
                    "sending_domain": "pkimber.net",
                    "count_sent": 101,
                    "count_bounce": 23,
                },
            ]
        },
    }


@pytest.mark.django_db
def test_create_sparkpost():
    sparkpost = SparkPost.objects._create_sparkpost("pkimber.net", 56, 23)
    sparkpost.refresh_from_db()
    assert 56 == sparkpost.count_sent
    assert 23 == sparkpost.count_bounce
    assert "pkimber.net" == sparkpost.domain


@pytest.mark.django_db
def test_current():
    SparkPostFactory(domain="z")
    SparkPostFactory(domain="a")
    assert ["a", "z"] == [x.domain for x in SparkPost.objects.current()]


@pytest.mark.django_db
def test_duplicate():
    SparkPostFactory(domain="https://www.pkimber.net/")
    with pytest.raises(IntegrityError) as e:
        SparkPostFactory(domain="https://www.pkimber.net/")


@pytest.mark.django_db
def test_init_sparkpost():
    SparkPost.objects._create_sparkpost("pkimber.net", 11, 23)
    SparkPost.objects.init_sparkpost("pkimber.net", 3, 11)
    assert 1 == SparkPost.objects.count()
    sparkpost = SparkPost.objects.first()
    assert 3 == sparkpost.count_sent
    assert 11 == sparkpost.count_bounce
    assert "pkimber.net" == sparkpost.domain


@pytest.mark.django_db
def test_monitor():
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="SparkPost"
    )
    assert 0 == SparkPost.objects.count()
    with HTTMock(sparkpost_response):
        result = monitor.monitor()
    assert 2 == result
    monitor.refresh_from_db()
    assert timezone.now().date() == monitor.checked.date()
    assert 2 == SparkPost.objects.count()
    x = SparkPost.objects.get(domain="kbsoftware.co.uk")
    assert 57 == x.count_sent
    assert 22 == x.count_bounce
    x = SparkPost.objects.get(domain="pkimber.net")
    assert 101 == x.count_sent
    assert 23 == x.count_bounce


@pytest.mark.django_db
def test_str():
    user = UserFactory(first_name="P", last_name="Kimber")
    domain = SparkPostFactory(
        domain="pkimber.net", count_sent=34, count_bounce=12
    )
    assert "pkimber.net sent 34 with 12 bounces" == str(domain)
