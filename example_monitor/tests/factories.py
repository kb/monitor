# -*- encoding: utf-8 -*-
import factory

from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from monitor.models import DomainName, Monitor, SparkPost


class DomainNameFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DomainName

    contact = factory.SubFactory(ContactFactory)
    found = True

    @factory.sequence
    def url(n):
        return "url_{:02d}".format(n)


class MonitorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Monitor

    @factory.sequence
    def hours(n):
        return n

    @factory.sequence
    def minutes(n):
        return n

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class SparkPostFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SparkPost

    @factory.sequence
    def count_bounce(n):
        return n

    @factory.sequence
    def count_sent(n):
        return n + 10

    @factory.sequence
    def domain(n):
        return "domain_{:02d}".format(n)
