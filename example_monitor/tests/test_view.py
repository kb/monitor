# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from monitor.models import DomainName
from .factories import DomainNameFactory, MonitorFactory


@pytest.mark.django_db
def test_contact_domain_create(client):
    """Create a domain name for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    url = reverse("monitor.contact.domain.create", args=[contact.pk])
    data = {"url": "https://www.pkimber.net/"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == DomainName.objects.count()
    domain_name = DomainName.objects.first()
    assert contact == domain_name.contact
    assert "https://www.pkimber.net/" == domain_name.url


@pytest.mark.django_db
def test_contact_domain_delete(client):
    """Delete a domain name for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    domain_name = DomainNameFactory(
        contact=contact, url="https://www.pkimber.net/"
    )
    assert 1 == DomainName.objects.current(contact).count()
    url = reverse("monitor.contact.domain.delete", args=[domain_name.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == DomainName.objects.count()
    assert 0 == DomainName.objects.current(contact).count()
    assert 0 == DomainName.objects.current().count()
    domain_name = DomainName.objects.first()
    assert domain_name.is_deleted is True


@pytest.mark.django_db
def test_contact_domain_update(client):
    """Update a domain name for a contact."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory(user=UserFactory())
    domain_name = DomainNameFactory(
        contact=contact, url="https://www.pkimber.net/"
    )
    assert 1 == DomainName.objects.count()
    url = reverse("monitor.contact.domain.update", args=[domain_name.pk])
    data = {"url": "https://www.pkimber.net/dash/"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("contact.detail", args=[contact.pk]) == response.url
    assert 1 == DomainName.objects.count()
    domain_name = DomainName.objects.first()
    assert contact == domain_name.contact
    assert "https://www.pkimber.net/dash/" == domain_name.url


@pytest.mark.django_db
def test_report_list(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    DomainNameFactory(url="a")
    DomainNameFactory(url="b").set_deleted(user)
    DomainNameFactory(url="c")
    monitor = MonitorFactory(
        slug="apple", app="monitor", module="models", report_class="DomainName"
    )
    url = reverse("monitor.report.list", args=["apple"])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "monitor" in response.context
    monitor = response.context["monitor"]
    assert "apple" == monitor.slug
    assert "domainname_list" in response.context
    qs = response.context["domainname_list"]
    assert ["a", "c"] == [x.url for x in qs]
