# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from httmock import all_requests, HTTMock
from http import HTTPStatus
from unittest import mock

from example_monitor.tests.factories import DomainNameFactory, MonitorFactory
from login.models import SYSTEM_GENERATED
from login.tests.factories import UserFactory
from monitor.models import Monitor
from monitor.tasks import check_domain_name, check_sparkpost


@all_requests
def sparkpost_response(url, request):
    return {
        "status_code": HTTPStatus.OK,
        "content": {
            "results": [
                {
                    "sending_domain": "kbsoftware.co.uk",
                    "count_sent": 101,
                    "count_bounce": 22,
                },
                {
                    "sending_domain": "pkimber.net",
                    "count_sent": 76,
                    "count_bounce": 23,
                },
            ]
        },
    }


class MockWhoIs:
    expiration_date = date.today()


@pytest.mark.django_db
def test_check_domain_name():
    DomainNameFactory()
    DomainNameFactory()
    MonitorFactory(
        slug=Monitor.DOMAIN_NAME,
        app="monitor",
        module="models",
        report_class="DomainName",
    )
    UserFactory(username=SYSTEM_GENERATED)
    with mock.patch("whois.whois") as m:
        m.return_value = MockWhoIs()
        result = check_domain_name()
    assert 2 == result


@pytest.mark.django_db
def test_check_sparkpost():
    MonitorFactory(
        slug=Monitor.SPARKPOST,
        app="monitor",
        module="models",
        report_class="SparkPost",
    )
    UserFactory(username=SYSTEM_GENERATED)
    with HTTMock(sparkpost_response):
        result = check_sparkpost()
    assert 2 == result
