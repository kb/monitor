# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.db import IntegrityError
from django.utils import timezone
from unittest import mock

from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from example_monitor.tests.factories import DomainNameFactory, MonitorFactory
from monitor.models import Monitor, MonitorError


class MockWhoIs:
    expiration_date = date.today()


class MockWhoIsEmptyList:
    expiration_date = []


class MockWhoIsList:
    expiration_date = [date.today()]


@pytest.mark.django_db
def test_create_monitor():
    monitor = Monitor.objects.create_monitor("s", "t", 1, 2, "a", "m", "c")
    assert "s" == monitor.slug
    assert "t" == monitor.title
    assert "a" == monitor.app
    assert "m" == monitor.module
    assert "c" == monitor.report_class


@pytest.mark.django_db
def test_current():
    MonitorFactory(slug="z")
    MonitorFactory(slug="m").set_deleted(UserFactory())
    MonitorFactory(slug="a")
    assert ["a", "z"] == [x.slug for x in Monitor.objects.current()]


@pytest.mark.django_db
def test_due():
    monitor = MonitorFactory(checked=timezone.now(), hours=0, minutes=1)
    assert timezone.now().date() == monitor.due().date()


@pytest.mark.django_db
def test_due_never_checked():
    monitor = MonitorFactory(checked=None)
    assert monitor.due() is None


@pytest.mark.django_db
def test_duplicate():
    MonitorFactory(slug="monitor")
    with pytest.raises(IntegrityError) as e:
        MonitorFactory(slug="monitor")


@pytest.mark.django_db
def test_init_monitor():
    monitor = Monitor.objects.init_monitor("s", "t", 1, 2, "a", "m", "c")
    assert "s" == monitor.slug
    assert "t" == monitor.title
    assert 1 == monitor.hours
    assert 2 == monitor.minutes
    assert "a" == monitor.app
    assert "m" == monitor.module
    assert "c" == monitor.report_class
    Monitor.objects.init_monitor("s", "t2", 3, 4, "a2", "m2", "c2")
    assert 1 == Monitor.objects.count()
    monitor = Monitor.objects.get(slug="s")
    assert "s" == monitor.slug
    assert "t2" == monitor.title
    assert 3 == monitor.hours
    assert 4 == monitor.minutes
    assert "a2" == monitor.app
    assert "m2" == monitor.module
    assert "c2" == monitor.report_class


@pytest.mark.django_db
def test_ordering():
    MonitorFactory(slug="z")
    MonitorFactory(slug="a")
    assert ["a", "z"] == [x.slug for x in Monitor.objects.all()]


@pytest.mark.django_db
def test_str():
    monitor = MonitorFactory(slug="a", title="Apple")
    assert "Apple ('a')" == str(monitor)


@pytest.mark.django_db
def test_monitor():
    DomainNameFactory()
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="DomainName"
    )
    with mock.patch("whois.whois") as m:
        m.return_value = MockWhoIs()
        result = monitor.monitor()
    assert 1 == result
    monitor.refresh_from_db()
    assert timezone.now().date() == monitor.checked.date()


@pytest.mark.django_db
def test_monitor_who_is_empty_list():
    DomainNameFactory()
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="DomainName"
    )
    with mock.patch("whois.whois") as m:
        m.return_value = MockWhoIsEmptyList()
        result = monitor.monitor()
    assert 1 == result


@pytest.mark.django_db
def test_monitor_who_is_list():
    DomainNameFactory()
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="DomainName"
    )
    with mock.patch("whois.whois") as m:
        m.return_value = MockWhoIsList()
        result = monitor.monitor()
    assert 1 == result


@pytest.mark.django_db
def test_report():
    DomainNameFactory(url="a")
    DomainNameFactory(url="b").set_deleted(UserFactory())
    DomainNameFactory(url="c")
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="DomainName"
    )
    qs = monitor.report()
    assert ["a", "c"] == [x.url for x in qs]


@pytest.mark.django_db
def test_update_invalid_class():
    monitor = MonitorFactory(
        app="monitor", module="models", report_class="MyClassDoesNotExist"
    )
    with pytest.raises(MonitorError) as e:
        monitor.monitor()
    assert "has no attribute" in str(e.value)
    assert "MyClassDoesNotExist" in str(e.value)


@pytest.mark.django_db
def test_warning():
    monitor = MonitorFactory()
    assert "has never been checked" == monitor.warning()


@pytest.mark.django_db
def test_warning_not_overdue():
    checked = timezone.now() + relativedelta(hours=-1, minutes=-7)
    monitor = MonitorFactory(hours=1, minutes=10, checked=checked)
    assert monitor.warning() is None


@pytest.mark.django_db
def test_warning_overdue():
    checked = timezone.now() + relativedelta(hours=-2)
    monitor = MonitorFactory(hours=1, minutes=0, checked=checked)
    assert "is overdue" == monitor.warning()
