# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from example_monitor.tests.factories import DomainNameFactory
from monitor.models import DomainName


@pytest.mark.django_db
def test_current():
    DomainNameFactory(url="z", found=True)
    DomainNameFactory(url="m").set_deleted(UserFactory())
    DomainNameFactory(url="a", found=False)
    assert ["a", "z"] == [x.url for x in DomainName.objects.current()]


@pytest.mark.django_db
def test_current_contact():
    contact = ContactFactory(user=UserFactory())
    DomainNameFactory(contact=contact, url="z", found=True)
    DomainNameFactory(contact=contact, url="m").set_deleted(UserFactory())
    DomainNameFactory(contact=ContactFactory(), url="q", found=False)
    DomainNameFactory(contact=contact, url="a", found=False)
    assert ["a", "z"] == [x.url for x in DomainName.objects.current(contact)]


@pytest.mark.django_db
def test_duplicate():
    contact = ContactFactory(user=UserFactory())
    DomainNameFactory(contact=contact, url="https://www.pkimber.net/")
    # complicated to add::
    #   unique_together = ("contact", "url")
    # because we soft-delete the domain names
    # with pytest.raises(IntegrityError) as e:
    DomainNameFactory(contact=contact, url="https://www.pkimber.net/")
    assert 2 == DomainName.objects.count()


@pytest.mark.django_db
def test_expires_soon():
    contact = ContactFactory(user=UserFactory())
    domain = DomainNameFactory(contact=contact, expiry=timezone.now().date())
    assert domain.expires_soon() is True


@pytest.mark.django_db
def test_expires_soon_20_days():
    contact = ContactFactory(user=UserFactory())
    days_20 = timezone.now() + relativedelta(days=20)
    domain = DomainNameFactory(contact=contact, expiry=days_20.date())
    assert domain.expires_soon() is True


@pytest.mark.django_db
def test_expires_soon_40_days():
    contact = ContactFactory(user=UserFactory())
    days_40 = timezone.now() + relativedelta(days=40)
    domain = DomainNameFactory(contact=contact, expiry=days_40.date())
    assert domain.expires_soon() is False


@pytest.mark.django_db
def test_expires_soon_past():
    contact = ContactFactory(user=UserFactory())
    days_20 = timezone.now() + relativedelta(days=-20)
    domain = DomainNameFactory(contact=contact, expiry=days_20.date())
    assert domain.expires_soon() is True


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url,url_to_find",
    [
        ("https://www.pkimber.net", "https://www.pkimber.net/"),
        ("https://www.pkimber.net/", "https://www.pkimber.net"),
        ("https://www.pkimber.net/", "https://www.pkimber.net/"),
        ("https://www.pkimber.net/", "pkimber.net"),
    ],
)
def test_for_url(url, url_to_find):
    contact = ContactFactory(user=UserFactory(username="pkimber"))
    domain = DomainNameFactory(contact=contact, url=url)
    assert ["pkimber"] == [
        x.contact.user.username for x in DomainName.objects.for_url(url_to_find)
    ]


@pytest.mark.django_db
def test_init():
    contact = ContactFactory()
    x = DomainName.objects.init_domain_name(contact, "www.pkimber.net")
    assert "www.pkimber.net" == x.url
    assert contact == x.contact


@pytest.mark.django_db
def test_init_already_exists():
    contact = ContactFactory()
    DomainNameFactory(contact=contact, url="www.pkimber.net")
    assert 1 == DomainName.objects.count()
    x = DomainName.objects.init_domain_name(contact, "www.pkimber.net")
    assert 1 == DomainName.objects.count()
    assert 1 == DomainName.objects.count()
    assert "www.pkimber.net" == x.url
    assert contact == x.contact


@pytest.mark.django_db
def test_ordering():
    u1 = UserFactory(username="u1")
    c1 = ContactFactory(user=u1)
    u2 = UserFactory(username="u2")
    c2 = ContactFactory(user=u2)
    DomainNameFactory(contact=c1, url="d1", found=False, expiry=None)
    DomainNameFactory(contact=c1, url="d2", expiry=date(2017, 12, 29))
    DomainNameFactory(contact=c2, url="d3", expiry=date(2017, 11, 29))
    DomainNameFactory(contact=c1, url="d4", expiry=date(2017, 10, 29))
    DomainNameFactory(contact=c2, url="d5", expiry=date(2017, 9, 29))
    assert ["d1", "d5", "d4", "d3", "d2"] == [
        x.url for x in DomainName.objects.all()
    ]


@pytest.mark.django_db
def test_set_expiry():
    contact = ContactFactory(user=UserFactory())
    domain = DomainNameFactory(contact=contact, found=False, expiry=None)
    expiry = date(2017, 12, 31)
    domain.set_expiry(expiry)
    domain.refresh_from_db()
    assert expiry == domain.expiry
    assert domain.found is True


@pytest.mark.django_db
def test_set_expiry_not_found():
    contact = ContactFactory(user=UserFactory())
    domain = DomainNameFactory(
        contact=contact, url="d1", found=True, expiry=date.today()
    )
    domain.set_expiry(None)
    domain.refresh_from_db()
    assert domain.expiry is None
    assert domain.found is False


@pytest.mark.django_db
def test_str():
    user = UserFactory(first_name="P", last_name="Kimber")
    domain = DomainNameFactory(
        contact=ContactFactory(user=user, company_name=""),
        url="pkimber.net",
        expiry=date(2017, 12, 31),
    )
    assert "pkimber.net (P Kimber) expires 31/12/2017" == str(domain)


@pytest.mark.django_db
def test_str_no_expiry():
    user = UserFactory(first_name="P", last_name="Kimber")
    domain = DomainNameFactory(
        contact=ContactFactory(user=user, company_name=""), url="pkimber.net"
    )
    assert "pkimber.net (P Kimber)" == str(domain)
