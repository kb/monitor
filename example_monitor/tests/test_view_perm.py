# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory, UserContactFactory
from login.tests.factories import UserFactory
from login.tests.fixture import perm_check
from .factories import DomainNameFactory, MonitorFactory


@pytest.mark.django_db
def test_contact_detail(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_domain_create(perm_check):
    contact = ContactFactory(user=UserFactory())
    url = reverse("monitor.contact.domain.create", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_domain_delete(perm_check):
    contact = ContactFactory(user=UserFactory())
    domain_name = DomainNameFactory(contact=contact)
    url = reverse("monitor.contact.domain.delete", args=[domain_name.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_domain_update(perm_check):
    contact = ContactFactory(user=UserFactory())
    domain_name = DomainNameFactory(contact=contact)
    url = reverse("monitor.contact.domain.update", args=[domain_name.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_monitor_list(perm_check):
    MonitorFactory()
    url = reverse("monitor.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_monitor_report_list(perm_check):
    DomainNameFactory()
    monitor = MonitorFactory(
        slug="apple", app="monitor", module="models", report_class="DomainName"
    )
    url = reverse("monitor.report.list", args=["apple"])
    perm_check.staff(url)
