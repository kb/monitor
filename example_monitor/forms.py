# -*- encoding: utf-8 -*-
from django import forms

from base.form_utils import FileDropInput, set_widget_required
from .models import ExampleDocument


class ExampleDocumentForm(forms.ModelForm):
    """Allow the user to upload a document"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        field = self.fields["document"]
        field.widget.attrs.update({"class": "pure-input-2-3"})
        set_widget_required(field)

    class Meta:
        model = ExampleDocument
        fields = ("document",)
        widgets = {"document": FileDropInput}
