# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, TemplateView

from base.view_utils import BaseMixin
from contact.views import ContactDetailMixin


class ContactDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactDetailMixin,
    BaseMixin,
    DetailView,
):
    pass


class DashView(LoginRequiredMixin, BaseMixin, TemplateView):

    template_name = "example/dash.html"


class HomeView(BaseMixin, TemplateView):

    template_name = "example/home.html"


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):

    template_name = "example/settings.html"
