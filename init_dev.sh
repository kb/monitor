#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

echo Drop database: $DATABASE_NAME
psql -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
psql -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"

echo Deleting files in 'media-private'
rm -rf ./media-private/

django-admin migrate --noinput
django-admin init_project
django-admin init_app_monitor

django-admin demo_data_login
django-admin demo_data_project

django-admin runserver 0.0.0.0:8000
