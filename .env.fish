# Set environment variables for this VE
source venv-monitor/bin/activate.fish
if command -q k3d
  echo "Using Kubernetes"
  set -x KUBECONFIG (k3d get-kubeconfig)
  set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x DATABASE_PASS "postgres"
  set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
  set -x REDIS_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  set -x REDIS_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-redis-master)
  echo "KUBECONFIG:" $KUBECONFIG
else
  set -x DATABASE_HOST ""
  set -x DATABASE_PASS ""
  set -x DATABASE_PORT "5432"
  set -x REDIS_HOST "localhost"
  set -x REDIS_PORT "6379"
end

set -x BACKUP_SSH_FOLDER "backup"
set -x BACKUP_SSH_HOSTNAME "aws.net"
set -x BACKUP_SSH_USER "yb"
set -x CLOUDSPACE_PASSWORD "CLOUDSPACE_PASSWORD"
set -x CLOUDSPACE_USERNAME "CLOUDSPACE_USERNAME"
set -x DATABASE_NAME "dev_app_monitor_$USER"
set -x DATABASE_USER "postgres"
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DIGITAL_OCEAN_TOKEN "DIGITAL_OCEAN_TOKEN"
set -x DJANGO_SETTINGS_MODULE "example_monitor.dev_$USER"
set -x DOMAIN "dev"
set -x LINODE_TOKEN "LINODE_TOKEN"
set -x LOG_FOLDER ""
set -x LOG_SUFFIX "dev"
set -x MAIL_TEMPLATE_TYPE "django"
set -x SECRET_KEY "the_secret_key"
set -x SPARKPOST_API_KEY "sparkpost_api_key"

source .private.fish

echo "BACKUP_SSH_FOLDER:" $BACKUP_SSH_FOLDER
echo "BACKUP_SSH_HOSTNAME:" $BACKUP_SSH_HOSTNAME
echo "BACKUP_SSH_USER" $BACKUP_SSH_USER
echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_PASS:" $DATABASE_PASS
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DIGITAL_OCEAN_TOKEN:" $DIGITAL_OCEAN_TOKEN
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
echo "LINODE_TOKEN:" $LINODE_TOKEN
echo "REDIS_HOST:" $REDIS_HOST
echo "REDIS_PORT:" $REDIS_PORT
echo "SPARKPOST_API_KEY:" $SPARKPOST_API_KEY
