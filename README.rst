Monitor
*******

Documentation
https://www.kbsoftware.co.uk/docs/app-monitor.html

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-monitor
  # or
  python3 -m venv venv-monitor
  source venv-monitor/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Release
=======

https://www.kbsoftware.co.uk/docs/
